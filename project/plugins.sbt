resolvers += "Jacoco4Sbt repo" at "http://repository-diversit.forge.cloudbees.com/release"

addSbtPlugin("de.johoop" % "jacoco4sbt" % "2.0.0")

addSbtPlugin("eu.diversit.sbt.plugin" % "webdav4sbt" % "1.2")

